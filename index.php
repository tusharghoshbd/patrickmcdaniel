
<?php  
       // require 'client-history.php';  client_history("PatrickMcDaniel/index.php")  
?>

<!DOCTYPE>
<html>
<head>
    
    <title>Professor Patrick McDaniel</title>



    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"> -->
    <link rel="stylesheet" type="text/css" href="css/Bootstrap-v3.0.0.css">
    <link rel="stylesheet" type="text/css" href="css/Font-Awesome-4.4.0.css">
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
   
   
     <link rel="stylesheet" href="css/highcharts.css">
    <link rel="stylesheet" href="css/dashboard.css">

    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
     <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    <script type="text/javascript" src="js/highcharts.js"></script>



    <script type="text/javascript">
    $(document).ready(function() {



        lineChart();

function lineChart() {
        $("#containerGraph").highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Annual Publications by Year'
                },
                xAxis: {
                    categories: [2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Number of Publications'
                    }
                },
                tooltip: {
                    headerFormat: '<span >{point.key}</span><table>',
                    pointFormat: '<tr style="font-size:11px"><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Journal',
                    data: [0, 28, 25, 20, 28, 28, 47, 79, 72, 45, 118, 190, 198, 115, 0]
                }, {
                    name: 'Conference',
                    data: [0, 25, 141, 164, 130, 255, 240, 230, 242, 256, 239, 230, 120, 149, 58]
                }, {
                    name: 'Other Indexing',
                    data: [25, 60, 40, 25, 160, 180, 176, 100, 262, 241, 205, 170, 110, 58, 0]
                }, {
                    name: 'Book',
                    data: [0, 0, 10, 19, 11, 2, 22, 2, 9, 6, 1, 5, 17, 0, 27]
                }, {
                    name: 'Chapter in Book',
                    data: [8, 8, 5, 18, 9, 16, 4, 1, 0, 2, 41, 5, 9, 7, 2]
                }]
            }


        ); //end of chart container
    }

    }); // end of documetn


    
    </script>
</head>

<body>
    <header class="header-part">
        <div style="background-color:#428bca;color:#fff;line-height:18Px;padding:3px 0;font-size:12Px !important;">
            <div class="" style="padding:  0 17px ">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="pull-left" style="padding-right:20px; font-weight: bold;">
                            <i class="fa fa-phone"></i>(814) 863-3599
                        </div>
                        <div class="pull-left" style="font-weight: bold;">
                            <i class="fa fa-envelope"></i><a href="mailto:mcdaniel@cse.psu.edu" style="color:#fff;"> mcdaniel@cse.psu.edu</a>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="pull-right">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" target="_blank"><i class="fa fa-facebook social-icons" style="color:#fff;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" target="_blank"><i class="fa fa-twitter social-icons" style="color:#fff;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" target="_blank"><i class="fa fa-google-plus social-icons" style="color:#fff;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" target="_blank"><i class="fa fa-linkedin social-icons" style="color:#fff;"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="Div1" class="wrapper">
            <!-- Fixed navbar -->
            <div class="navi navbar-default" role="navigation">
                <div class="" style="padding:  0 17px ">
                    <div class="row">
                        

                   
                    <div  class="col-lg-4 col-md-4 col-sm-12" style="margin-top: 5px;">
                        <a href="index.php"><span class="text-size-small text-bold">HOME</span></a> / 
                        <!-- <a href="index.html"><span class="text-size-small text-bold">PROFILE</span></a> / 
                         <a href="index.html"><span class="text-size-small text-bold">PUBLICATION</span></a> -->

                    </div>
                    <div  class="col-lg-8 col-md-8 col-sm-12">
                       <!--  <marquee behavior="alternate" style="" direction="right" scrollamount="2" loop="-1">
                                            <center>
                                              <blink>Welcome To Professor Dato' Dr. Hassan Said
                                                  personal Web page</blink> 
                                            </center>
                                          </marquee> -->
                    </div>
                     
                    <div style="clear:both;"></div>
                     </div>
                </div>
            </div>
            <!-- End of Nav -->
        </div>
    </header>

    <!--Start Content-->
    <div class="" style="padding:10px 0 0 0;">
        <div id="" class="col-xs-12 col-sm-12">
            <!--Start Dashboard Content-->
            <div class="row">
                <!-- Middle Content [Left]-->
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="panel panel-overflow  ">
                        <div class="profile-upper profile-background-image" style="border-top: 3px solid #428bca" >
                            <img src="image/headshot-07-2015.png" alt="avatar" class="img-circle img-thumbnail img-thumbnail-profile-2x">



                            <h3 class="profile-title">Professor Patrick McDaniel</h3>
                            <h4 class="text-size-small">Computer Science and Engineering</h4>
                            <h4 class="text-size-small">Pennsylvania State University</h4>
                        </div>
                        <div class="profile-footer" style="padding: 0px">
                            <table class="table table-hover personal-info">
                                <tbody>
                                    <tr>
                                        <td class="menu-left-icon"><i class="fa fa-tachometer"></i></td>
                                        <td style="width: 80%; border-right: 1px solid #428BCA" >
                                            <a href="index.php"><span class="text-size-small text-bold">HOME</span></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-user"></i></td>
                                        <td style="width: 80%" ><a href="profile.php"><span class="text-size-small text-bold">PROFILE</span></a></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-tasks"></i></td>
                                        <td style="width: 80%"><a href="#"><span class="text-size-small text-bold">PUBLICATIONS</span></a></td>
                                    </tr>
                                  <!--   <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-briefcase"></i></td>
                                        <td style="width: 80%"><a href="#"><span class="text-size-small text-bold">WORK EXPERIENCE</span></a></td>
                                    </tr> -->
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-graduation-cap"></i></td>
                                        <td style="width: 80%"><a href="#"><span class="text-size-small text-bold">EDUCATION</span></a></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-phone"></i></td>
                                        <td style="width: 80%"><a href="#"><span class="text-size-small text-bold">CONTACT</span></a></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-edit"></i></td>
                                        <td style="width: 80%"><a href="#" target="_blank"><span class="text-size-small text-bold">VIEW CV</span></a></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class=" fa fa-info"></i></td>
                                        <td style="width: 80%">
                                            <a data-toggle="collapse" href="#collapse1" class="text-size-small text-bold">BIOGRAPHY</a>
                                        </td>
                                    </tr>
                                    <tr style="margin:-3em;">
                                        <td colspan="2">
                                            <div id="collapse1" class="well panel-collapse collapse m-0 f-12 text-justify">
                                                <p class="MsoNormal" style="text-align: justify; line-height: normal; margin: 0in 0in 0pt; mso-layout-grid-align: none;">
                                                    <span style="font-family: Calibri;">
                                            <span style="font-size: 14px; mso-bidi-font-family: AdvPS405B6;">
                                         <p style="text-align: justify;"><font size="2">I</font> was born in Spokane, Washington, and moved to
                                            Athens, Ohio at an early age.  I grew up in Athens, where I eventually went to
                                            <a href="http://www.ohiou.edu/" target="_top">Ohio University</a> for my
                                            undergraduate education (joining the <a href="http://www.deltau.org/">Delta
                                            Upsilon Social Fraternity</a> ).  After completing my B.S. in Computer Science
                                            at Ohio, I attended <a href="http://www.bsu.edu/" target="_top">Ball State
                                            University</a>, where I completed a M.S. in Computer Science. I moved to San
                                            Diego, California, working at Primary Access (<em>now, 3COM/Primary
                                            Access</em>).  After three years of living in Cardiff, I moved to Columbus
                                            Ohio, where I worked for <a href="http://www.aiinet.com" target="_top">Applied
                                            Innovation</a>.  In the fall of 1996, I moved to Ann Arbor, Michigan, where I
                                            began attending the <a href="http://www.umich.edu/" target="_top">University
                                            of Michigan</a>, pursuing a Ph.D. in Electrical Engineering and Computer
                                            Science. After completing the Ph.D. in the Fall of 2001, I moved to
                                            Morristown, New Jersey where I worked at AT&amp;T--Research in Florham Park, NJ
                                            (previously known as Bell Labs).  I moved in August 2004 to State College,
                                            PA and joined the faculty at Penn State University.  I have lived there ever since.</p>
                                         </span>
                                                    </span>
                                                </p>
                                                <br />
                                                <div class="row text-center">
                                                    <a data-toggle="collapse" href="#collapse1" class="btn btn-info btn-xs"><i class="fa fa-times" aria-hidden="true"></i> <span>Close</span></a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel panel-overflow">
                        <!--<div class="panel panel-border-top widget-publication">-->
                        <div class="panel-heading widget-h bl-default">
                            <span>Bibliometric Data</span>
                        </div>
                        <div class="panel-body " style="padding-top: 0px; padding-bottom: 0px;">
                            <table class="table table-hover personal-info">
                                <tbody>
                                    <tr>
                                        <td width="5%"><i class="fa fa-paperclip"></i></td>
                                        <td><span class="text-size-small text-bold">Total Articles in Publication List</span></td>
                                        <td width="10%"><span class="text-size-small text-bold">152</span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-paperclip"></i></td>
                                        <td><span class="text-size-small text-bold">Articles With Citation Data</span></td>
                                        <td><span class="text-size-small text-bold">152</span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-paperclip"></i></td>
                                        <td><span class="text-size-small text-bold">Sum of the Times Cited</span></td>
                                        <td><span class="text-size-small text-bold">174</span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-paperclip"></i></td>
                                        <td><span class="text-size-small text-bold">Average Citations per Article</span></td>
                                        <td><span class="text-size-small text-bold">7.35</span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-paperclip"></i></td>
                                        <td><span class="text-size-small text-bold">H-Index</span></td>
                                        <td><span class="text-size-small text-bold">35</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    
                    <!-- <div class="row">
                        <div class="col-xs-6 col-sm-4 col-md-6 col-lg-6">
                            <div class="thumbnail text-center m-b-25">
                                <div class="panel-body widget-awards">
                                    <div class="widget-top">
                                        <span class="widget-title">Latest Award</span>
                                    </div>
                                    <div class="circle-container" style="min-height: 150px">
                                        <img src="image/award.png">
                                    </div>
                                    <div class="widget-awards-content">
                                        <span class="mt-15 mb-5 widget-label elipsis-2-line">
                        Anugerah perkhidmatan cemerlang                 </span>
                                        <div class="text-size-small text-muted f-w-500">Year 2015</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-xs-6 col-sm-4 col-md-6 col-lg-6">
                            <div class="thumbnail text-center m-b-25 activitystat">
                                <div class="panel-body widget-activity">
                                    <div class="widget-top">
                                        <span class="widget-title">Latest Evaluation</span>
                                    </div>
                                    <div class="circle-container" style="min-height: 150px">
                                         <img src="image/award.png">
                                    </div>
                                    <div class="widget-activity-content">
                                        <span class="mt-15 mb-5 widget-label elipsis-2-line">
                        Reviewer                    </span>
                                        <div class="text-size-small text-muted f-w-500">Year 2013 - 2015</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  -->
                </div>
                


                <div class="col-md-8 col-sm-12 p-0">

                    <div class="row" style="margin: 0px">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="panel widget-statistics">
                                <!-- <div class="panel-heading widget-h bl-default">
                                    <span>Publication Statistics</span>
                                     <ul class="nav panel-tabs stat-tabs">
                                        <li class="active"><a href="#publication" data-toggle="tab" data-identifier="statPublication">Publication</a></li>
                                    </ul> 
                                </div> -->
                                <div class="panel-body p-b-10 p-t-10">
                                    <div class="tab-content p-0" >
                                     
                                    <div id="containerGraph" style="height: 400px" ></div>
                                    </div>
                                </div>
                                <!-- / widget content -->
                            </div>
                            <!-- / .panel -->
                        </div>
                    </div>

                    <div class="row" style="margin: 0px">
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            <div class="panel panel-overflow">
                                <!--<div class="panel panel-border-top widget-publication">-->
                                <div class="panel-heading widget-h bl-default">
                                    <span>Last 4 Projects Status</span>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                        <thead>
                                            <tr style="font-size: 12px">
                                                <th width="40%">Grant</th>
                                                <th width="55%">Progress</th>
                                                <th width="5%">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <span class="widget-label">
                                                    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Title : Quran And Hadith Authentication Systems" class="black-tooltip">Project 1</a>
                                                </span>
                                            </td>
                                            <td>
                                                <div class="progress progress-striped active" style="height:10px; margin:5px 0 0 0;">
                                                    <div class="progress-bar progress-bar-success" style="width:100%"></div>
                                                </div>                                                  
                                            </td>
                                            <td><span class="label text-size-small label-success">end</span></td>
                                        </tr>
                                                                        <tr>
                                            <td>
                                                <span class="widget-label">
                                                    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Title : CONTEXT-BASED KEYWORD PATTERN CLUSTER ANALYSIS TECHNIQUE ON TACIT KNOWLEDGE OF MILITARY TACTICS CENTER OF EXPERTISE" class="black-tooltip">Project 2</a>
                                                </span>
                                            </td>
                                            <td>
                                                <div class="progress progress-striped active" style="height:10px; margin:5px 0 0 0;">
                                                    <div class="progress-bar progress-bar-success" style="width:100%"></div>
                                                </div>                                                  
                                            </td>
                                            <td><span class="label text-size-small label-success">end</span></td>
                                        </tr>
                                                                        <tr>
                                            <td>
                                                <span class="widget-label">
                                                    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Title : Big Data And Mobile Cloud For Collaborative Experiments" class="black-tooltip">Project 3</a>
                                                </span>
                                            </td>
                                            <td>
                                                <div class="progress progress-striped active" style="height:10px; margin:5px 0 0 0;">
                                                    <div class="progress-bar progress-bar-success" style="width:100%"></div>
                                                </div>                                                  
                                            </td>
                                            <td><span class="label text-size-small label-success">end</span></td>
                                        </tr>
                                                                        <tr>
                                            <td>
                                                <span class="widget-label">
                                                    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Title : Computational History - Recreating The Past of Kuala Terengganu Port" class="black-tooltip">Project 4</a>
                                                </span>
                                            </td>
                                            <td>
                                                <div class="progress progress-striped active" style="height:10px; margin:5px 0 0 0;">
                                                    <div class="progress-bar progress-bar-success" style="width:100%"></div>
                                                </div>                                                  
                                            </td>
                                            <td><span class="label text-size-small label-success">end</span></td>
                                        </tr>
                                    </tbody>
                                    </table>
                                    </div>
                                </div><!-- / widget content -->
                                 <div class="panel-footer text-center text-size-small f-w-500">
                                    <span style="font-size: 12px">This information is generated from Research Management System</span>
                                </div>
                            </div><!-- / .panel -->
                        </div>  
                        <div class="col-lg-6 col-md-12 col-sm-12">
                           <div class="panel panel-overflow">
                                <!--<div class="panel panel-border-top widget-publication">-->
                                <div class="panel-heading widget-h bl-default">
                                    <span>key research area</span> <span style="font-size: 8px"> ( name, no of publication )</span>
                                </div>
                                <div class="panel-body " style="padding: 0px; ">
                                    <div class="box-body">
                                          <ul class="todo-list ui-sortable">
                                            <li>
                                              <!-- drag handle -->
                                                  <span class="handle ui-sortable-handle">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                  </span>
                                              <!-- todo text -->
                                              <span class="text">Cyber-Security Collaborative Research Alliance,</span>
                                              <!-- Emphasis label -->
                                              <small class="label label-danger">
                                              <i class="fa fa-tasks"></i> 4 Pubs</small>
                                              <!-- General tools such as edit or delete-->
                                              
                                            </li>
                                            <li>
                                                  <span class="handle ui-sortable-handle">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                  </span>
                                              <span class="text">Smartphone Application Security,</span>
                                              <small class="label label-info"><i class="fa fa-tasks"></i> 15 Pubs</small>
                                              
                                            </li>
                                            <li>
                                                  <span class="handle ui-sortable-handle">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                  </span>
                                              <span class="text">Storage Security and Data Provenance,</span>
                                              <small class="label label-warning">
                                              <i class="fa fa-tasks"></i> 6 Pubs</small>
                                              
                                            </li>
                                            <li>
                                                  <span class="handle ui-sortable-handle">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                  </span>
                                              <span class="text">SmartGrid and Control Systems Security,</span>
                                              <small class="label label-success"><i class="fa fa-tasks"></i> 12 Pubs</small>
                                             
                                            </li>
                                            <li>
                                                  <span class="handle ui-sortable-handle">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                  </span>
                                              <span class="text">Network Security,</span>
                                              <small class="label label-primary"><i class="fa fa-tasks"></i> 18 Pubs</small>
                                              
                                            </li>
                                            <li>
                                                  <span class="handle ui-sortable-handle">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                  </span>
                                              <span class="text">Telecommunications Security,</span>
                                              <small class="label label-default"><i class="fa fa-tasks"></i> 14 Pubs</small>
                                              
                                            </li>
                                          </ul>
                                        </div>
                                </div>
                            </div>  
                        </div>
                    </div>

                    

                    <div class="row" style="margin: 0px">
                       <div class="col-lg-12 col-md-12 col-sm-12">
                           <div class="panel panel-overflow"> 
                                        <div class="panel-heading widget-h bl-default ">
                                            <h3 class="panel-title ">
                                               <span class="hidden-xs"> Latest Publication</span>
                                               <span class="visible-xs"> Publs</span>
                                            </h3> 
                                            <br class="visible-xs">
                                            <span class="pull-right">
                                                <!-- Tabs -->
                                                <ul class="nav panel-tabs">
                                                    <li  class="active">
                                                        <a href="#tab2" data-toggle="tab" title="Journal Publications">J.P.</a>
                                                    </li>

                                                   <li >
                                                        <a href="#tab5" data-toggle="tab" title="Conference Publications">C.P.</a>
                                                    </li>
                                                    
                                                    
                                                     <li >
                                                        <a href="#tab1" data-toggle="tab" title="Books">Books</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab3" data-toggle="tab" title="Book Chapters">B.C.</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab4" data-toggle="tab" title="Columns">Columns</a>
                                                    </li>
                                                </ul>
                                            </span>
                                        </div>
                                        <div class="panel-body" style="padding: 0px;  ">
                                            <div class="tab-content">



                                                <div class="tab-pane " id="tab5" ">

                                                    <table class="table table-striped dataTable no-footer" id="list_of_pub_J" style="font-size: 12px"  role="grid" aria-describedby="list_of_pub_J_info">
                                                    <thead>
                                                        <tr role="row" valign="top" ><th class="sorting_disabled" rowspan="1" colspan="1" width="3%">NO</th><th class="sorting_disabled" rowspan="1" colspan="1">DETAILS OF CONFERENCE PUBLICATIONS</th></tr>
                                                    </thead>
                                                    <tbody >
                                                        <tr role="row" class="even">
                                                            <td>1.</td>
                                                            <td>
                                                                   Nicolas Papernot, Patrick McDaniel, Ian Goodfellow, Somesh Jha, Z. Berkay Celik, and Ananthram Swami. Practical Black-Box Attacks against Machine Learning. ACM Asia Conference on Computer and Communications Security (ASIACCS) 2017    &nbsp;
                                                                <span class="label label-success">April </span> &nbsp;
                                                                <span class="label label-success">2017 </span>
                                                            </td>
                                                        </tr>

                                                        <tr role="row" class="odd">
                                                            <td>2.</td>
                                                            <td>
                                                                    Stefan Achleitner, Thomas La Porta, Trent Jaeger, and Patrick McDaniel. Adversarial Network Forensics in Software Defined Networking. ACM Symposium on SDN Research (SOSR),&nbsp;
                                                                    <span class="label label-success">ACM</span>&nbsp;
                                                                    <span class="label label-success">April </span>&nbsp;
                                                                    <span class="label label-success">2017</span>
                                                            </td>
                                                        </tr>
                                                        <tr role="row" class="even">
                                                            <td>3.</td>
                                                            <td>
                                                                    Ahmed Fathy Atya, Zhiyun Qian, Srikanth V. Krishnamurthy, Thomas La Porta, Patrick McDaniel, and Lisa Marvel. Stealth Migration: Hiding Virtual Machines on the Network. IEEE International Conference on Computer Communications (INFOCOM)&nbsp;
                                                                    <span class="label label-success">IEEE 2017</span>
                                                                
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="bottom">
                                                        <div class="dataTables_info" style="text-align: center;">
                                                            <a href="#">View All</a>
                                                        </div>
                                                    </div>
                                                </div>




                                                <div class="tab-pane" id="tab1" ">

                                                    <table class="table table-striped dataTable no-footer" id="list_of_pub_J" style="font-size: 12px"  role="grid" aria-describedby="list_of_pub_J_info">
                                                    <thead>
                                                        <tr role="row" valign="top" ><th class="sorting_disabled" rowspan="1" colspan="1" width="3%">NO</th><th class="sorting_disabled" rowspan="1" colspan="1">DETAILS OF BOOKS</th></tr>
                                                    </thead>
                                                    <tbody >
                                                        <tr role="row" class="even">
                                                            <td>1.</td>
                                                            <td>
                                                                   Patrick Traynor, Patrick McDaniel, and Thomas La Porta, Security for Telecommunications Networks. Springer, Series: Advances in Information Security&nbsp;  
                                                                    <span class="label label-success">July</span>
                                                                 &nbsp;<span class="label label-success">2008</span>
                                                                 &nbsp;<span class="label label-success">ISBN: 978-0-387-72441-6</span>

                                                            </td>
                                                        </tr>

                                                        
                                                        </tbody>
                                                    </table>
                                                    <div class="bottom">
                                                        <div class="dataTables_info" style="text-align: center;">
                                                            <a href="#">View All</a>
                                                        </div>
                                                    </div>
                                                </div>





                                                <div class="tab-pane active" id="tab2">
                                                    

                                                    <table style="font-size: 12px" class="table table-striped dataTable no-footer" id="list_of_pub_J" role="grid" aria-describedby="list_of_pub_J_info">
                                                            <thead>
                                                                <tr role="row" valign="top"><th class="sorting_disabled" rowspan="1" colspan="1" width="3%">NO</th><th class="sorting_disabled" rowspan="1" colspan="1">DETAILS OF JOURNAL PUBLICATION</th></tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr role="row" class="even">
                                                                    <td>1.</td>
                                                                    <td>
                                                                         Chaz Lever, Robert J. Walls, Yacin Nadji, David Dagon, Patrick McDaniel, and Manos Antonakakis, Dawn of the Dead Domain: Measuring the Exploitation of Residual Trust in Domains. IEEE Security & Privacy Magazine (Secure Systems issue column).&nbsp;
                                                                            <span class="label label-success">2017</span>
                                                                            <!-- <span class="label label-success">143-165</span> -->
                                                                    </td>
                                                                </tr>

                                                                <tr role="row" class="odd">
                                                                    <td>2.</td>
                                                                    <td>
                                                                            Patrick McDaniel and Ananthram Swami, The Cyber Security Collaborative Research Alliance:Unifying Detection, Agility, and Risk in Mission-Oriented Cyber Decision Making. CSIAC Journal, Army Research Laboratory (ARL) Cyber Science and Technology, 5(1)&nbsp;
                                                                            <span class="label label-success">December</span>
                                                                            <span class="label label-success"> 2016</span>
                                                                     
                                                                    </td>
                                                                </tr>
                                                                <tr role="row" class="even">
                                                                    <td>3.</td>
                                                                    <td>
                                                                            Damien Octeau, Daniel Luchaup, Somesh Jha, and Patrick McDaniel, Composite Constant Propagation and its Application to Android Program Analysis. IEEE Transactions on Software Engineering
                                                                            <span class="label label-success">42(11):999-1014</span>
                                                                            <span class="label label-success">2016</span>
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                    </table>
                                                    <div class="bottom">
                                                            <div class="dataTables_info" style="text-align: center;">
                                                                <a href="#">View All</a>
                                                            </div>
                                                    </div>


                                                </div>



                                                <div class="tab-pane" id="tab3">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</div>

                                                <div class="tab-pane" id="tab4">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

                                                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet,</div>
                                            </div>
                                        </div>
                                    </div>
                       </div>
                   </div>
                   <div class="row" style="margin: 0px">
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            <div class="thumbnail text-center m-b-25">
                                <div class="panel-body widget-awards">
                                    <div class="widget-top">
                                        <span class="widget-title">Latest Award</span>
                                    </div>
                                    <div class="circle-container" style="min-height: 150px">
                                        <img src="image/award.png">
                                    </div>
                                    <div class="widget-awards-content">
                                        <span class="mt-15 mb-5 widget-label elipsis-2-line">
                       Award-1               </span>
                                        <div class="text-size-small text-muted f-w-500">Year 2015</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            <div class="thumbnail text-center m-b-25">
                                <div class="panel-body widget-awards">
                                    <div class="widget-top">
                                        <span class="widget-title">Latest Award</span>
                                    </div>
                                    <div class="circle-container" style="min-height: 150px">
                                        <img src="image/award.png">
                                    </div>
                                    <div class="widget-awards-content">
                                        <span class="mt-15 mb-5 widget-label elipsis-2-line">
                        Award-2                </span>
                                        <div class="text-size-small text-muted f-w-500">Year 2015</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            <div class="thumbnail text-center m-b-25 activitystat">
                                <div class="panel-body widget-activity">
                                    <div class="widget-top">
                                        <span class="widget-title">Latest Evaluation</span>
                                    </div>
                                    <div class="circle-container" style="min-height: 150px">
                                         <img src="image/award.png">
                                    </div>
                                    <div class="widget-activity-content">
                                        <span class="mt-15 mb-5 widget-label elipsis-2-line">
                        Reviewer                    </span>
                                        <div class="text-size-small text-muted f-w-500">Year 2013 - 2015</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            <div class="thumbnail text-center m-b-25 activitystat">
                                <div class="panel-body widget-activity">
                                    <div class="widget-top">
                                        <span class="widget-title">Latest Evaluation</span>
                                    </div>
                                    <div class="circle-container" style="min-height: 150px">
                                         <img src="image/award.png">
                                    </div>
                                    <div class="widget-activity-content">
                                        <span class="mt-15 mb-5 widget-label elipsis-2-line">
                        Reviewer                    </span>
                                        <div class="text-size-small text-muted f-w-500">Year 2013 - 2015</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 

                    
                </div>
                <!-- End Middle Content [Right] -->
            </div>
            <!--End Dashboard Content-->
        </div>
    </div>







    <div style="background-color:#333333;color:#fff;line-height:50Px;padding:3px 0;font-size:12Px !important;">
            <div class="" style="padding:  0 17px ">
                <div class="row">
                    <div class="col-lg-6 col-sm-10 col-xs-10">
                        
                         Copyright &copy; patrickmcdaniel.org | 
                         <a href="http://siis.cse.psu.edu">SIISLab</a> | 
                        <a href="http://www.cse.psu.edu">CSE</a> | 
                        <a href="http://www.psu.edu">PSU</a> 

                        <div class="clear"></div>
                    </div>
                    <div class="col-lg-6 col-sm-2 col-xs-2">
                        <div class="pull-right">
                            <a href="#"><i class="fa fa-arrow-circle-up" style="color:#fff; font-size: 20px;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>



</body>
</html>