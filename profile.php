<!DOCTYPE>
<html>
<head>
    
    <title>Professor Patrick McDaniel</title>



    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"> -->
    <link rel="stylesheet" type="text/css" href="css/Bootstrap-v3.0.0.css">
    <link rel="stylesheet" type="text/css" href="css/Font-Awesome-4.4.0.css">
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
   
   
     <link rel="stylesheet" href="css/highcharts.css">
    <link rel="stylesheet" href="css/dashboard.css">

    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
     <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    <script type="text/javascript" src="js/highcharts.js"></script>



    <script type="text/javascript">
    $(document).ready(function() {


    }); // end of documetn


    
    </script>
</head>

<body>
    <header class="header-part">
        <div style="background-color:#428bca;color:#fff;line-height:18Px;padding:3px 0;font-size:12Px !important;">
            <div class="" style="padding:  0 17px ">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="pull-left" style="padding-right:20px; font-weight: bold;">
                            <i class="fa fa-phone"></i>(814) 863-3599
                        </div>
                        <div class="pull-left" style="font-weight: bold;">
                            <i class="fa fa-envelope"></i><a href="mailto:mcdaniel@cse.psu.edu" style="color:#fff;"> mcdaniel@cse.psu.edu</a>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="pull-right">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" target="_blank"><i class="fa fa-facebook social-icons" style="color:#fff;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" target="_blank"><i class="fa fa-twitter social-icons" style="color:#fff;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" target="_blank"><i class="fa fa-google-plus social-icons" style="color:#fff;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" target="_blank"><i class="fa fa-linkedin social-icons" style="color:#fff;"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="Div1" class="wrapper">
            <!-- Fixed navbar -->
            <div class="navi navbar-default" role="navigation">
                <div class="" style="padding:  0 17px ">
                    <div class="row">
                        

                   
                    <div  class="col-lg-4 col-md-4 col-sm-12" style="margin-top: 5px;">
                        <a href="index.php"><span class="text-size-small text-bold">HOME</span></a> / 
                         <a href="profile.php"><span class="text-size-small text-bold">PROFILE</span></a> / 
                         <!--<a href="index.html"><span class="text-size-small text-bold">PUBLICATION</span></a> -->

                    </div>
                    <div  class="col-lg-8 col-md-8 col-sm-12">
                       <!--  <marquee behavior="alternate" style="" direction="right" scrollamount="2" loop="-1">
                                            <center>
                                              <blink>Welcome To Professor Dato' Dr. Hassan Said
                                                  personal Web page</blink> 
                                            </center>
                                          </marquee> -->
                    </div>
                     
                    <div style="clear:both;"></div>
                     </div>
                </div>
            </div>
            <!-- End of Nav -->
        </div>
    </header>
    <!--Start Content-->
    <div class="" style="padding:10px 0 0 0;">
        <div id="" class="col-xs-12 col-sm-12">
            <!--Start Dashboard Content-->
            <div class="row">
                <!-- Middle Content [Left]-->
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="panel panel-overflow  ">
                        <div class="profile-upper profile-background-image" style="border-top: 3px solid #428bca" >
                           <!--  <div>
                                <img src="https://bootstrap-themes.github.io/application/assets/img/instagram_13.jpg">
                            </div> -->
                            <img src="image/headshot-07-2015.png" alt="avatar" class="img-circle img-thumbnail img-thumbnail-profile-2x">



                            <h3 class="profile-title">Professor Patrick McDaniel</h3>
                            <h4 class="text-size-small">Computer Science and Engineering</h4>
                            <h4 class="text-size-small">Pennsylvania State University</h4>
                        </div>
                        <div class="profile-footer" style="padding: 0px">
                            <table class="table table-hover personal-info">
                                <tbody>
                                    <tr>
                                        <td class="menu-left-icon" ><i class="fa fa-tachometer"></i></td>
                                        <td style="width: 80%;" >
                                            <a href="index.php"><span class="text-size-small text-bold">HOME</span></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-user"></i></td>
                                        <td style="width: 80%" class="menu-selected"><a href="profile.php" >
                                        <span class="text-size-small text-bold">PROFILE</span></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-tasks"></i></td>
                                        <td style="width: 80%"><a href="#"><span class="text-size-small text-bold">PUBLICATIONS</span></a></td>
                                    </tr>
                                    <!-- <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-briefcase"></i></td>
                                        <td style="width: 80%"><a href="#"><span class="text-size-small text-bold">WORK EXPERIENCE</span></a></td>
                                    </tr> -->
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-graduation-cap"></i></td>
                                        <td style="width: 80%"><a href="#"><span class="text-size-small text-bold">EDUCATION</span></a></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-phone"></i></td>
                                        <td style="width: 80%"><a href="#"><span class="text-size-small text-bold">CONTACT</span></a></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-edit"></i></td>
                                        <td style="width: 80%"><a href="#" target="_blank"><span class="text-size-small text-bold">VIEW CV</span></a></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class=" fa fa-info"></i></td>
                                        <td style="width: 80%">
                                            <a data-toggle="collapse" href="#collapse1" class="text-size-small text-bold">BIOGRAPHY</a>
                                        </td>
                                    </tr>
                                    <tr style="margin:-3em;">
                                        <td colspan="2">
                                            <div id="collapse1" class="well panel-collapse collapse m-0 f-12 text-justify">
                                                <p class="MsoNormal" style="text-align: justify; line-height: normal; margin: 0in 0in 0pt; mso-layout-grid-align: none;">
                                                    <span style="font-family: Calibri;">
                                            <span style="font-size: 14px; mso-bidi-font-family: AdvPS405B6;">
                                        <p style="text-align: justify;"><font size="2">I</font> was born in Spokane, Washington, and moved to
                                        Athens, Ohio at an early age.  I grew up in Athens, where I eventually went to
                                        <a href="http://www.ohiou.edu/" target="_top">Ohio University</a> for my
                                        undergraduate education (joining the <a href="http://www.deltau.org/">Delta
                                        Upsilon Social Fraternity</a> ).  After completing my B.S. in Computer Science
                                        at Ohio, I attended <a href="http://www.bsu.edu/" target="_top">Ball State
                                        University</a>, where I completed a M.S. in Computer Science. I moved to San
                                        Diego, California, working at Primary Access (<em>now, 3COM/Primary
                                        Access</em>).  After three years of living in Cardiff, I moved to Columbus
                                        Ohio, where I worked for <a href="http://www.aiinet.com" target="_top">Applied
                                        Innovation</a>.  In the fall of 1996, I moved to Ann Arbor, Michigan, where I
                                        began attending the <a href="http://www.umich.edu/" target="_top">University
                                        of Michigan</a>, pursuing a Ph.D. in Electrical Engineering and Computer
                                        Science. After completing the Ph.D. in the Fall of 2001, I moved to
                                        Morristown, New Jersey where I worked at AT&amp;T--Research in Florham Park, NJ
                                        (previously known as Bell Labs).  I moved in August 2004 to State College,
                                        PA and joined the faculty at Penn State University.  I have lived there ever since.</p>
                                                                                 </span>
                                                    </span>
                                                </p>
                                                <br />
                                                <div class="row text-center">
                                                    <a data-toggle="collapse" href="#collapse1" class="btn btn-info btn-xs"><i class="fa fa-times" aria-hidden="true"></i> <span>Close</span></a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel panel-overflow">
                        <!--<div class="panel panel-border-top widget-publication">-->
                        <div class="panel-heading widget-h bl-default">
                            <span>Bibliometric Data</span>
                        </div>
                        <div class="panel-body " style="padding-top: 0px; padding-bottom: 0px;">
                            <table class="table table-hover personal-info">
                                <tbody>
                                    <tr>
                                        <td width="5%"><i class="fa fa-paperclip"></i></td>
                                        <td><span class="text-size-small text-bold">Total Articles in Publication List</span></td>
                                        <td width="10%"><span class="text-size-small text-bold">52</span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-paperclip"></i></td>
                                        <td><span class="text-size-small text-bold">Articles With Citation Data</span></td>
                                        <td><span class="text-size-small text-bold">52</span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-paperclip"></i></td>
                                        <td><span class="text-size-small text-bold">Sum of the Times Cited</span></td>
                                        <td><span class="text-size-small text-bold">174</span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-paperclip"></i></td>
                                        <td><span class="text-size-small text-bold">Average Citations per Article</span></td>
                                        <td><span class="text-size-small text-bold">3.35</span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-paperclip"></i></td>
                                        <td><span class="text-size-small text-bold">H-Index</span></td>
                                        <td><span class="text-size-small text-bold">7</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    
                </div>
                <!-- End Middle Content [Left]-->
                <!-- Middle Content [Right] -->
                <div class="col-md-8 col-sm-12 p-0">
                    <!-- Start Widget Statistics -->
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="panel widget-statistics">
                            <!-- <div class="panel-heading widget-h bl-default">
                                <span>Publication Statistics</span>
                                 <ul class="nav panel-tabs stat-tabs">
                                    <li class="active"><a href="#publication" data-toggle="tab" data-identifier="statPublication">Publication</a></li>
                                </ul> 
                            </div> -->
                            <div class="panel-body p-b-10 p-t-10">
                                <div class="tab-content p-0" >
                                    <p>I am a Distinguished Professor in the <a href="http://www.eecs.psu.edu/">School of Electrical Engineering and Computer Science</a> at
                                    <a href="http://www.psu.edu/">Pennsylvania State University</a> and a fellow of both <a href="https://www.ieee.org/index.html">IEEE</a> and <a href="http://www.acm.org">ACM</a>.  I am also the
                                    Director of the Institute for Networking and Security Research (<a href="http://insr.psu.edu/">INSR</a>), a research institute focused on
                                    the study of networking and security in diverse computing environments.
                                    </p>


                                    <o>
                                    I am also the Program Manager (PM) and lead scientist for the newly
                                    created <a href="http://cra.psu.edu/">Cyber Security (CS) Collaborative
                                    Research Alliance (CRA)</a>.  The CRA is led by <a href="http://www.psu.edu/">Penn State University</a> and includes faculty and researchers the <a href="">Army Research Laboratory</a>, <a href="http://www.cmu.edu/index.shtml">Carnegie Mellon University</a>, <a href="http://www.indiana.edu/">Indiana University</a>, the <a href="http://www.ucdavis.edu/">University of California-Davis</a>, and
                                    the <a href="http://www.ucr.edu/">University of
                                    California-Riverside</a>.  In addition, an industrial partner will be
                                    selected to participate in the coming months.<p></p>

                                    <p>
                                    My professional life is devoted to the pursuit of novel research in a
                                    broad array of areas of computer science (see below).  As part of that
                                    pursuit I am fortunate to advise a number of exceptionally talented and
                                    committed graduate students and post-docs, as well as participate in many rewarding
                                    professional service activities.  This website documents many of these
                                    activities, but is not even close to exhaustive.  If there is a
                                    something you wish to know or want more detail on information that is
                                    here, feel free to contact me directly.
                                    </p>

                                    <p>
                                    The pages linked from the menu at the left cover broadly
                                    my research, teaching, and personal activities.  Those wishing to
                                    speak to me in a professional capacity should contact me via email.
                                    Email is probably the most reliable way of contacting me (warning,
                                    responses may take a couple of days due to travel and other factors).
                                    </p>


                                    <h3 class="SECTION">Students</h3>


                                      <b>Past Post-Docs</b><br><ul class="minbullets"><li><a href="http://pages.cs.wisc.edu/~vrastogi/">Vaibhav Rastogi</a>
                                              (Northwestern University)</li>
                                    <li><a href="http://rjwalls.github.io/">Robert Walls</a>
                                              (University of Massachusetts, Amherst)</li>
                                    </ul>
                                      
                                      <b>Past PhD Students</b><br><ul class="minbullets"><li><a href="http://www.wenhuihu.org">Wenhui Hu</a>
                                              (2016), now </li>
                                    <li><a href="http://www.cse.psu.edu/people/djp284">Devin Pohly</a>
                                              (2016), now 
                                              Assistant Professor
                                              Wheaton College
                                            </li>
                                    <li><a href="http://www.cse.psu.edu/~duo114/">Damien Octeau</a>
                                              (2014), now 
                                              Software Engineer in Security
                                              Google
                                            </li>
                                    <li><a href="http://www.cse.psu.edu/~smclaugh/">Steve McLaughlin</a>
                                              (2014), now 
                                              Senior Software Engineer
                                              Samsung Research America
                                            </li>
                                    <li><a href="http://www.cse.psu.edu/~tmmoyer/">Thomas Moyer</a>
                                              (2011), now 
                                              Technical Staff
                                              Massachusetts Institute of Technology-Lincoln Labs
                                            </li>
                                    <li><a href="http://www.enck.org/">William Enck</a>
                                              (2011), now 
                                              Assistant Professor
                                              North Carolina State University
                                            </li>
                                    <li><a href="http://www.kevinbutler.org/">Kevin Butler</a>
                                              (2010), now 
                                              Associate Professor
                                              University of Florida
                                            </li>
                                    <li><a href="http://www.cse.psu.edu/~ongtang/">Machigar Ongtang</a>
                                              (2010), now 
                                              Assistant Professor
                                              Dhurakij Pundit University
                                            </li>
                                    <li><a href="https://www.cise.ufl.edu/~traynor/">Patrick Traynor</a>
                                              (2008), now 
                                              Associate Professor
                                              University of Florida
                                            </li>
                                    <li><a href="http://cis.stvincent.edu/hicksb/">Fr. Boniface Hicks</a>
                                              (2007), now 
                                              Assistant Professor
                                              St. Vincent College
                                            </li>
                                    </ul>

                                      <b>Current PhD Students</b><br><ul class="minbullets"><li><a href="http://www.cse.psu.edu/~zbc102/">Z. Berkay Celik</a> (2019), <a href="http://www.papernot.fr/">Nicolas Papernot</a> (2019)</li></ul>

                                      


                                      





                                    <h3 class="SECTION">Professional Miscellanea/Highlights</h3>

                                    <ul class="minbullets">

                                    <li>I am a Distinguished Professor in the <a href="http://www.eecs.psu.edu/">School of Electrical Engineering and Computer Science</a> at
                                    <a href="http://www.psu.edu/">Pennsylvania State University</a>.

                                    </li><li>
                                    I am the Director of the Institute for Networking and Security Research (<a href="http://insr.psu.edu/">INSR</a>).

                                    </li><li>
                                    I am a fellow of the <a href="http://www.ieee.org/index.html">Institute of Electrical and Electronics Engineers (IEEE)</a>.

                                    </li><li>
                                    I am a fellow of the <a href="http://www.acm.org/index.html">Association for Computing Machinery (ACM)</a>.


                                    </li><li>
                                    I am the Program Manager (PM) and lead scientist for thhe <a href="http://cra.psu.edu/">Cyber Security (CS) Collaborative
                                    Research Alliance (CRA)</a>

                                    </li><li>I am the form co-director of the <a href="http://siis.cse.psu.edu/">Systems
                                    and Internet Infrastructure Security Laboratory</a>.

                                    </li><li>I was the Chair of the <a href="http://www.ieee-security.org/&quot;">IEEE Computer Society's Technical Committee on Security and Privacy</a> from January 2014 to December 2015.</li>

                                    <li>I was the co-chair of the <a href="http://www.ieee-security.org/TC/SP2007/oakland07-home.html">2007</a> and
                                    <a href="http://www.ieee-security.org/TC/SP2008/oakland08-home.html">2008 IEEE
                                    Symposium on Security and Privacy</a>.

                                    </li><li>I was the principal investigator of the academic <a href="http://www.patrickmcdaniel.org/pubs/everest.pdf">Everest</a> team which analyized the voting eqiupment used in Ohio.  The
                                    report can be retrieved <a href="http://www.sos.state.oh.us/sos/info/everest.aspx?Section=3180">
                                    here</a>, and the team's commentary <a href="docs/everest-statement.pdf">here</a>.

                                    </li><li>I was the chair of the <a href="http://www.usenix.org/publications/library/proceedings/sec05/">2005
                                    USENIX Security Symposium</a>.

                                    </li><li>I am an alumnus of the <a href="http://www.research.att.com/p_rareas.cfm?aid=2&amp;type=2,3">Secure
                                    Systems Group</a> at <a href="http://www.research.att.com/">AT&amp;T
                                    Research</a>.

                                    </li><li>I received my PhD from the <a href="http://www.eecs.umich.edu/">Electrical Engineering and Computer
                                    Science Department</a> at the <a href="http://www.umich.edu/">University of Michigan</a> in December of
                                    2001.

                                    </li><li>As a doctoral student, I received the <a href="http://www.nasa.gov/">NASA</a>, <a href="http://www.ksc.nasa.gov/">Kennedy Space Center</a> Fellowship.

                                    </li><li>My <a href="http://www.oakland.edu/enp/">Erdös number</a> is 3
                                    (Paul Erdös--Fan Chung--William Aiello--Me).</li>

                                    <li>I have an <a href="http://www.arrl.org/">amateur radio license</a> and my call letters are KC2LNP.</li>

                                    <li>I received my <a href="http://www.padi.com/scuba/padi-courses/diver-level-courses/view-all-padi-courses/scuba-diver/default.aspx">open water scuba diver certification</a> in March of 2013.</li>

                                    <li>I started batch number 1341 at the <a href="http://www.guinness.com/">Guinness</a> factory in <a href="http://www.visitdublin.com/">Dublin, Ireland</a> on 22nd of
                                    June, 2007.</li>

                                    </ul>

                                    <p>I also keep a <a href="http://patrickmcdaniel.wordpress.com/">personal blog</a> where I discuss a number of topics of interest that relate to my life within and outside of the academy.</p>


                                        </o>    
                                </div>
                            </div>
                            <!-- / widget content -->
                        </div>
                        <!-- / .panel -->
                    </div>

                    
                </div>
                <!-- End Middle Content [Right] -->
            </div>
            <!--End Dashboard Content-->
        </div>
    </div>







    <div style="background-color:#333333;color:#fff;line-height:50Px;padding:3px 0;font-size:12Px !important;">
            <div class="" style="padding:  0 17px ">
                <div class="row">
                    <div class="col-lg-6 col-sm-10 col-xs-10">
                          Copyright &copy; patrickmcdaniel.org | 
                         <a href="http://siis.cse.psu.edu">SIISLab</a> | 
                        <a href="http://www.cse.psu.edu">CSE</a> | 
                        <a href="http://www.psu.edu">PSU</a> 

                        <div class="clear"></div>
                    </div>
                    <div class="col-lg-6 col-sm-2 col-xs-2">
                        <div class="pull-right">
                            <a href="#"><i class="fa fa-arrow-circle-up" style="color:#fff; font-size: 20px;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>



</body>
</html>